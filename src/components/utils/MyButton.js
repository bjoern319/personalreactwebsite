import React, { Component } from 'react'
import Button from '@material-ui/core/Button'

export default class MyButton extends Component {
  render() {
    return (
      <div>
        <Button
          href={this.props.link}
          variant="contained"
          size="small"
          style={{
            background: this.props.background,
            color: this.props.color
          }}
        >
          {this.props.text}
        </Button>
      </div>
    )
  }
}
