import React, { Component } from 'react'
import Fade from 'react-reveal/Fade'

export default class Footer extends Component {
  render() {
    return (
      <footer className="bck_red">
        <Fade delay={1000}>
          <div className="font_righteous footer_logo_title">Björn Geisler</div>
          <div className="footer_copyright">
            Björn Geisler 2018. All rights reserved.
          </div>
        </Fade>
      </footer>
    )
  }
}
