import React from 'react'
import { scroller } from 'react-scroll'

import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'

const SideDrawer = props => {
  const scrollToElement = element => {
    scroller.scrollTo(element, {
      duration: 1500,
      delay: 100,
      smooth: true,
      offset: -150
    })
    props.onClose(false)
  }

  return (
    <Drawer
      anchor="right"
      open={props.open}
      onClose={() => props.onClose.false}
    >
      <List component="nav">
        <ListItem button onClick={() => scrollToElement('Home')}>
          Home
        </ListItem>
        <ListItem button onClick={() => scrollToElement('Kurzinfo')}>
          Kurzinfo
        </ListItem>
        <ListItem button onClick={() => scrollToElement('Details')}>
          Details
        </ListItem>
        <ListItem button onClick={() => scrollToElement('Portfolio')}>
          Portfolio
        </ListItem>
        <ListItem button onClick={() => scrollToElement('Location')}>
          Location
        </ListItem>
        <ListItem button onClick={() => scrollToElement('Footer')}>
          Footer
        </ListItem>
      </List>
    </Drawer>
  )
}

export default SideDrawer
