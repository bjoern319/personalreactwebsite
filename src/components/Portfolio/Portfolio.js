import React, { Component } from 'react'
import Zoom from 'react-reveal/Zoom'

export default class Portfolio extends Component {
  state = {
    pictures: ['pic1', 'pic2', 'pic3', 'pic4'],
    names: ['TriciGo', 'ImmoOnDisplay', 'CarOnDisplay', 'SocialLife'],
    desc: [
      'Diese von mir als AbschlussProjekt von "neue fische" entwickelte ReiseApp nutzt die Technologien React und Redux, die Flickr-API und einen Server',
      'Dieses von mir gebaute Anzeigenportal nutzt die Technologien React und Redux sowie MongoDB mit Mongoose',
      'Dieses von mir gebaute Anzeigenportal nutzt die Technologien React und Redux sowie MongoDB mit Mongoose',
      'Dieses von mir entwickelte Social Media Protal nutzt verschiedenste Technologien'
    ],
    delay: [500, 100, 500, 500]
  }

  showBoxes = () =>
    this.state.pictures.map((box, i) => (
      <Zoom delay={this.state.delay[i]} key={i}>
        <div className="portfolio_item">
          <div className="portfolio_inner_wrapper">
            <div className="portfolio_title">
              <span>{this.state.pictures[i]}</span>
              <span>{this.state.names[i]}</span>
            </div>
            <div className="portfolio_description">{this.state.desc[i]}</div>
          </div>
        </div>
      </Zoom>
    ))

  render() {
    return (
      <div className="bck_black">
        <div className="center_wrapper portfolio_section">
          <h2>Portfolio</h2>
          <div className="portfolio_wrapper">{this.showBoxes()}</div>
        </div>
      </div>
    )
  }
}
