import React, { Component } from 'react'
import Fade from 'react-reveal/Fade'
import Slide from 'react-reveal/Slide'

export default class CV extends Component {
  render() {
    return (
      <div className="center_wrapper">
        <div className="cv_wrapper">
          <Fade>
            <div className="cv_years">
              <span>2018-2019</span>
            </div>
          </Fade>
          <Slide>
            <div className="cv_description">
              <h3>
                Studium der Betriebswirtschaftslehre mit Schwerpunkt Marketing
                an der Hamburger Fern-Hochschule
              </h3>
            </div>
          </Slide>
        </div>

        <div className="cv_wrapper">
          <Fade>
            <div className="cv_years">
              <span>2015-2018</span>
            </div>
          </Fade>
          <Slide>
            <div className="cv_description">
              <h3>
                Studium der Betriebswirtschaftslehre mit Schwerpunkt Marketing
                und Controling an der Hochschule Wismar.
              </h3>
            </div>
          </Slide>
        </div>

        <div className="cv_wrapper">
          <Fade>
            <div className="cv_years">
              <span>2010-2015</span>
            </div>
          </Fade>
          <Slide>
            <div className="cv_description">
              <h3>
                Studium der Verfahrenstechnik und Biotechnologie an der
                HAW-Hamburg
              </h3>
            </div>
          </Slide>
        </div>
      </div>
    )
  }
}
