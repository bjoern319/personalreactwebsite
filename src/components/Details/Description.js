import React, { Component } from 'react'
import Zoom from 'react-reveal/Zoom'

export default class Description extends Component {
  render() {
    return (
      <Zoom delay={500}>
        <div className="center_wrapper">
          <h2>Details</h2>
          <div className="details_description">
            Hallo, ich bin Björn Geisler und studiere Betriebswirtschaftslehre
            mit Schwerpunkt Marketing im Fernstudium. Da es mir wichtig ist mich
            stätig weiter zu entwickeln habe ich neben dem Fernstudium eine
            Weiterbildung zum <strong>Online-Marketing Manager (IHK)</strong>{' '}
            und ein <strong>Web Development</strong> Bootcamp bei "neue fische"
            absolviert. Seit dem ist das Entwickeln mit React und Redux mein
            Hobby.
          </div>
        </div>
      </Zoom>
    )
  }
}
