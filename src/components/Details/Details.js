import React, { Component } from 'react'
import Description from './Description'
import CV from './CV'

export default class Details extends Component {
  render() {
    return (
      <div className="details_wrapper">
        <Description />
        <CV />
      </div>
    )
  }
}
