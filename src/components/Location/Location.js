import React, { Component } from 'react'

export default class Location extends Component {
  render() {
    return (
      <div className="location_wrapper">
        <iframe
          title="location"
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4273.117952383332!2d9.701753709950992!3d53.57988732137079!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b17898b2987ac7%3A0x330a0aabf27ee77d!2sWedel!5e0!3m2!1sde!2sde!4v1540067883413"
          width="100%"
          height="500"
          frameBorder="0"
          allowFullScreen
        />
      </div>
    )
  }
}
