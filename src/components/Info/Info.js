import React, { Component } from 'react'
import icon_calendar from '../../resources/images/icons/calendar.png'
import icon_location from '../../resources/images/icons/location.png'
import icon_person from '../../resources/images/icons/person.png'
import icon_development from '../../resources/images/icons/development.png'
import icon_marketing from '../../resources/images/icons/marketing.png'
import Zoom from 'react-reveal/Zoom'

export default class Info extends Component {
  render() {
    return (
      <div className="bck_black">
        <div className="center_wrapper">
          <div className="vn_wrapper">
            <Zoom>
              <div className="vn_item">
                <div className="vn_outer">
                  <div className="vn_inner">
                    <div className="vn_icon_square bck_red" />
                    <div
                      className="vn_icon"
                      style={{
                        background: `url(${icon_person})`
                      }}
                    />
                    <div className="vn_title">Hallo ich bin Björn</div>
                    <div className="vn_desc">
                      Traveler, BWL Fernstudent mit Schwerpunkt Marketing,
                      Online-Marketing Manager (IHK), Web Developer in Training
                    </div>
                  </div>
                </div>
              </div>
            </Zoom>
            <Zoom delay={400}>
              <div className="vn_item">
                <div className="vn_outer">
                  <div className="vn_inner">
                    <div className="vn_icon_square bck_red" />
                    <div
                      className="vn_icon"
                      style={{
                        background: `url(${icon_development})`
                      }}
                    />
                    <div className="vn_title"> Web Development Skills</div>
                    <div className="vn_desc">
                      React, Redux, SCSS, Styled Components ES6, ES7, Firebase,
                      MongoDB, Mongoose{' '}
                    </div>
                  </div>
                </div>
              </div>
            </Zoom>
            <Zoom delay={800}>
              <div className="vn_item">
                <div className="vn_outer">
                  <div className="vn_inner">
                    <div className="vn_icon_square bck_red" />
                    <div
                      className="vn_icon"
                      style={{
                        background: `url(${icon_marketing})`
                      }}
                    />
                    <div className="vn_title"> Marketing Skills</div>
                    <div className="vn_desc">
                      Online-Marketing Manager IHK, Erstellung vollständiger
                      Marketing-konzeptionen
                    </div>
                  </div>
                </div>
              </div>
            </Zoom>
          </div>
        </div>
      </div>
    )
  }
}
