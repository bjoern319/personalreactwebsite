import React, { Component } from 'react'
import './resources/styles.css'
import { Element } from 'react-scroll'

import Header from './components/header_footer/Header'
import Featured from './components/featured/Featured'
import Info from './components/Info/Info'
import Details from './components/Details/Details'
import Portfolio from './components/Portfolio/Portfolio'
import Location from './components/Location/Location'
import Footer from './components/header_footer/Footer'

class App extends Component {
  render() {
    return (
      <div
        className="App"
        style={{ height: '150vh', background: 'cornflowerblue' }}
      >
        <Header />
        <Element name="Home">
          <Featured />
        </Element>
        <Element name="Kurzinfo">
          <Info />
        </Element>
        <Element name="Details">
          <Details />
        </Element>
        <Element name="Portfolio" t>
          <Portfolio />
        </Element>
        <Element name="Location">
          <Location />
        </Element>
        <Element name="Footer">
          <Footer />
        </Element>
      </div>
    )
  }
}

export default App
